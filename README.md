je reste sur ma fain
====================
Pierre GURIEL--FARDEL

Installation
------------

1/ Clone the repository (or download it, idc)
```
git clone https://gitlab.com/pyguri/je-reste-sur-ma-fain.git ~/Desktop/Best_FAIN_Project
```

2/ Generate the makefile using cmake
```
cmake .
```

3/ Build the executable
```
make all
```

4/ Run the program with or without parameters (without, the default parameters will be used)
```
./je-reste-sur-ma-fain <height> <width>
```

Using the program
-----------------

#### Draw modes
Cycle through drawing modes using the `space` bar. Additionally, you can press `p` to toggle the display of the points.

#### Edit modes
**Add at tail :** press `a` and left click to add point at the end of the polygon.

**Point selection :** press `s` and left click to select the nearest point to your cursor, press `backspace` to remove it and automatically select another point.

**Edge selection :** press `e` and left click to select the nearest edge to your cursor, press `r` to create a new point in the middle of the edge.