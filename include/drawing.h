//
// Created by davion on 23/10/2019.
//

#ifndef TP2_BRESENHAM_H
#define TP2_BRESENHAM_H

#include "Image.h"

#define DRAW_STRIP (unsigned) 1
#define DRAW_LOOP (unsigned) 2
#define DRAW_FILL (unsigned) 4
#define DRAW_POINT (unsigned) 8

/**
 * Our point type, has coordinates.
 * Only has utility in a polygon, so next (and previous) necessary.
 */
typedef struct PolyPoint {
    int x;
    int y;
    struct PolyPoint *next;
    struct PolyPoint *prev;
} poly_point;

/**
 * The polygon type, has a number of points.
 * Works as a linked list, head (and tail) are therefore used.
 * ymin/max are used for scan line filling
 */
typedef struct Polygon {
    int size;
    poly_point *head;
    poly_point *tail;
    int ymin;
    int ymax;
} polygon;

/**
 * Constructor for a polygon
 * @return pointer to the newly created polygon
 */
struct Polygon *newPolygon();

/**
 * Constructor for a polygon point
 * @param x horizontal coordinate
 * @param y vertical coordinate
 * @return pointer to the newly created poly point
 */
poly_point *newPolyPoint(int x, int y);

/**
 * Query a point at the given index of a polygon
 * @param pPolygon the polygon we search in
 * @param i index of the point
 * @return the pointer to the point in the polygon
 */
struct PolyPoint *getPoint(struct Polygon *pPolygon, int i);

/**
 * Query the point of the polygon that is the nearest from the given coordinates
 * @param pPolygon the polygon we search in
 * @param x horizontal coordinate
 * @param y vertical coordinate
 * @return the pointer of the nearest point in the polygon
 */
struct PolyPoint *getNearestPoint(struct Polygon *pPolygon, int x, int y);

/**
 * Query the point of the polygon that forms the nearest edge from the given coordinates.
 * A point forms an edge with its successor.
 * @param pPolygon the polygon we search in
 * @param x horizontal coordinate
 * @param y vertical coordinate
 * @param mode the drawing mode the polygon is in, if it loops, we need to check the tail->head edge
 * @return the pointer of the point forming the nearest edge
 */
struct PolyPoint *getNearestEdge(struct Polygon *pPolygon, int x, int y, unsigned mode);

/**
 * Create and add a new point in the middle of a segment formed by the given point in the polygon.
 * A point forms an edge with its successor
 * @param pPolygon the polygon to update
 * @param point the point forming the edge
 * @param mode the drawing mode can be DRAW_STRIP, DRAW_LOOP or DRAW_FILL
 */
void addMidPoint(struct Polygon *pPolygon, struct PolyPoint *point, unsigned mode);

/**
 * Create and add a new point at the end of the polygon (the tail) at the given coordinates
 * @param pPolygon the polygon we update
 * @param x horizontal coordinate
 * @param y vertical coordinate
 */
void addTail(struct Polygon *pPolygon, int x, int y);

/**
 * Add a new point to the given coordinates in the given polygon, update ymin/max if necessary
 * @param pPolygon The polygon to add the new point to
 * @param point The point from the polygon the new point will take the place of, can be NULL.
 * If point is NULL then the new point will be added at the tail of the polygon
 * @param x,y coordinates of the new point
 */
void addPoint(struct Polygon *pPolygon, struct PolyPoint *point, int x, int y);

/**
 * Remove a point from a polygon
 * @param pPolygon the polygon to update
 * @param point the point to remove from the polygon
 */
void removePolyPoint(struct Polygon *pPolygon, struct PolyPoint *point);

/**
 * Remove the point from the polygon at the given index
 * @param pPolygon the polygon we update
 * @param i the index of the point to remove
 */
void removePointIndex(struct Polygon *pPolygon, int i);

/**
 * Free a polygon with its points
 * @param pPolygon the polygon to free
 */
void freePolygon(struct Polygon *pPolygon);

/**
 * Draw a polygon in an image
 * @param image the image to draw in
 * @param pPolygon the polygon to draw
 * @param mode the drawing mode can be DRAW_STRIP, DRAW_LOOP or DRAW_FILL
 */
void I_polygon(Image *image, polygon *pPolygon, unsigned mode);

/**
 * Draw a square at the coordinates of the given point
 * @param img the image the image to draw in
 * @param pPoint the point to draw
 * @param size the size of the square to draw, in pixel
 */
void I_point(Image *img, struct PolyPoint *pPoint, int size);

/**
 * Draw a single edge of a polygon using a point and its successor
 * @param img the image the image to draw in
 * @param pPolygon the polygon we use
 * @param pPoint the first point of the edge
 * @param mode the drawing mode can be DRAW_STRIP, DRAW_LOOP or DRAW_FILL
 */
void I_edge(Image *img, struct Polygon *pPolygon, struct PolyPoint *pPoint, unsigned mode);

#endif //TP2_BRESENHAM_H
