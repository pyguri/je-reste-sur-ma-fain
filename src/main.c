/*===============================================================*\

	Arash Habibi

	main.c

	Un programme equivalent à 02_glut.c et qui ne prend en compte
	que trois événements pour quitter le programme.

\*===============================================================*/

#include <stdio.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include "drawing.h"

#include "Image.h"

#define ADD_TAIL (unsigned) 1
#define SELECT_NODE (unsigned) 2
#define SELECT_EDGE (unsigned) 4


Image *img;
struct Polygon *poly = NULL;
struct PolyPoint *selected_node = NULL;
unsigned draw_mode = DRAW_STRIP;
unsigned edit_mode = ADD_TAIL;

Color gris, blanc;

//------------------------------------------------------------------
//	C'est le display callback. A chaque fois qu'il faut
//	redessiner l'image, c'est cette fonction qui est
//	appelee. Tous les dessins doivent etre faits a partir
//	de cette fonction.
//------------------------------------------------------------------

void display_CB() {
    glClear(GL_COLOR_BUFFER_BIT);
    I_checker(img, gris, blanc, 10);

    I_polygon(img, poly, draw_mode);

    if(selected_node != NULL){
        if (edit_mode & SELECT_NODE)
            I_point(img, selected_node, 10);
        if (edit_mode & SELECT_EDGE)
            I_edge(img, poly, selected_node, draw_mode);
    }

    I_draw(img);

    glutSwapBuffers();
}

//------------------------------------------------------------------
// Cette fonction permet de réagir en fonction de la position de
// la souris (x,y), en fonction du bouton de la souris qui a été
// pressé ou relaché.
//------------------------------------------------------------------

void mouse_CB(int button, int state, int x, int y) {
    if ((button == GLUT_LEFT_BUTTON) && (state == GLUT_UP))
        switch (edit_mode) {
            case ADD_TAIL :
                addTail(poly, x, img->_height - y);
                break;
            case SELECT_NODE :
                selected_node = getNearestPoint(poly, x, img->_height - y);
                break;
            case SELECT_EDGE :
                selected_node = getNearestEdge(poly, x, img->_height - y, draw_mode);
                break;
            default:
                break;
        }
    glutPostRedisplay();
}

//------------------------------------------------------------------
// Cette fonction permet de réagir au fait que l'utilisateur
// presse une touche (non-spéciale) du clavier.
//------------------------------------------------------------------

void keyboard_CB(unsigned char key, int x, int y) {
    // fprintf(stderr,"key=%d\n",key);
    poly_point *next = NULL;
    switch (key) {
        case 27 :
            exit(1);
        case 'z' :
            I_zoom(img, 2.0);
            break;
        case 'Z' :
            I_zoom(img, 0.5);
            break;
        case 'i' :
            I_zoomInit(img);
            break;
        case 's' :
            printf("Edit mode : POINT SELECTION\n");
            edit_mode = SELECT_NODE;
            selected_node = NULL;
            break;
        case 'e' :
            printf("Edit mode : EDGE SELECTION\n");
            edit_mode = SELECT_EDGE;
            selected_node = NULL;
            break;
        case 'a' :
            printf("Edit mode : ADDING TAIL\n");
            edit_mode = ADD_TAIL;
            selected_node = NULL;
            break;
        case 8 :
            if (selected_node != NULL && (edit_mode & SELECT_NODE)){
                next = selected_node->next;
                removePolyPoint(poly, selected_node);
            }
            selected_node = next;
            break;
        case 'r' :
                if (selected_node != NULL && (edit_mode & SELECT_EDGE))
                    addMidPoint(poly, selected_node, draw_mode);
            break;
        case ' ':
            switch (draw_mode){
                case DRAW_STRIP:
                    draw_mode = DRAW_LOOP;
                    printf("Draw mode : LOOP\n");
                    break;
                case DRAW_LOOP:
                    draw_mode = DRAW_FILL;
                    printf("Draw mode : FILL\n");
                    break;
                default:
                    draw_mode = DRAW_STRIP;
                    printf("Draw mode : STRIP\n");
                    break;
            }
            break;
        case 'p' :
            if (draw_mode & DRAW_POINT) {
                printf("hide points\n");
                draw_mode -= DRAW_POINT;
            }
            else {
                printf("show points\n");
                draw_mode += DRAW_POINT;
            }
            break;
        default :
            fprintf(stderr, "keyboard_CB : %d : unknown key. x = %i, y = %i\n", key, x, y);
    }
    glutPostRedisplay();
}

//------------------------------------------------------------------
// Cette fonction permet de réagir au fait que l'utilisateur
// presse une touche spéciale (F1, F2 ... F12, home, end, insert,
// haut, bas, droite, gauche etc).
//------------------------------------------------------------------

void special_CB(int key, int x, int y) {
    // int mod = glutGetModifiers();

    int d = 10;

    switch (key) {
        case GLUT_KEY_UP    :
            I_move(img, 0, d);
            break;
        case GLUT_KEY_DOWN  :
            I_move(img, 0, -d);
            break;
        case GLUT_KEY_LEFT  :
            I_move(img, d, 0);
            break;
        case GLUT_KEY_RIGHT :
            I_move(img, -d, 0);
            break;
        default :
            fprintf(stderr, "special_CB : %d : unknown key, x = %i, y = %i\n", key, x, y);
    }
    glutPostRedisplay();
}

//------------------------------------------------------------------------


int main(int argc, char **argv) {
    char *title = "500*500";
    int largeur = 500, hauteur = 500;

    if (argc == 2) {
        title = malloc(strlen(argv[1]) * sizeof(char));
        strcpy(title, argv[1]);
        img = I_read(argv[1]);
        largeur = img->_width;
        hauteur = img->_height;
    }

    if (argc == 3) {
        title = malloc((strlen(argv[1]) + 1 + strlen(argv[2]) + 1) * sizeof(char));
        sprintf(title, "%s*%s", argv[1], argv[2]);
        largeur = (int) strtol(argv[1], NULL, 10);
        hauteur = (int) strtol(argv[2], NULL, 10);
    }

    img = I_new(largeur, hauteur);

    I_changeColor(img, C_new(0.0f, 0.0f, 0.0f));
    gris = C_new(0.4f, 0.4f, 0.4f);
    blanc = C_new(0.7f, 0.7f, 0.7f);

    int windowPosX = 100, windowPosY = 100;

    poly = newPolygon();

    glutInitWindowSize(largeur, hauteur);
    glutInitWindowPosition(windowPosX, windowPosY);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInit(&argc, argv);
    glutCreateWindow(title);

    glViewport(0, 0, largeur, hauteur);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glOrtho(0, largeur, 0, hauteur, -1, 1);

    glutDisplayFunc(display_CB);
    glutKeyboardFunc(keyboard_CB);
    glutSpecialFunc(special_CB);
    glutMouseFunc(mouse_CB);
    // glutMotionFunc(mouse_move_CB);
    // glutPassiveMotionFunc(passive_mouse_move_CB);

    glutMainLoop();

    freePolygon(poly);

    return 0;
}
