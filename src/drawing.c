//
// Created by davion on 23/10/2019.
//

#include <GL/glut.h>
#include <math.h>
#include "Image.h"
#include "drawing.h"

polygon *newPolygon() {
    polygon *result = (polygon *) malloc(sizeof(polygon));
    result->head = NULL;
    result->tail = NULL;
    result->size = 0;
    result->ymin = (int) INFINITY;
    result->ymax = -1;
    return result;
}

poly_point *newPolyPoint(int x, int y) {
    poly_point *result = (poly_point *) malloc(sizeof(poly_point));
    result->next = NULL;
    result->prev = NULL;

    result->x = x;
    result->y = y;

    return result;
}

struct PolyPoint *getPoint(struct Polygon *pPolygon, int i) {
    poly_point *result = pPolygon->head;

    if (i >= pPolygon->size)
        result = pPolygon->tail;
    else
        for (int j = 0; j < i; ++j)
            result = result->next;

    return result;
}

double distance(int x1, int y1, int x2, int y2) {
    return sqrt(pow((double) x1 - x2, 2.0) + pow((double) y1 - y2, 2.0));
}

struct PolyPoint *getNearestPoint(struct Polygon *pPolygon, int x, int y) {
    poly_point *result = pPolygon->head;
    double min_dist = distance(result->x, result->y, x, y);
    double dist;

    poly_point *currPoint = pPolygon->head;
    while (currPoint != NULL) {

        dist = distance(currPoint->x, currPoint->y, x, y);

        if (dist < min_dist) {
            result = currPoint;
            min_dist = dist;
        }
        currPoint = currPoint->next;
    }

    return result;
}

double compute_alpha(poly_point *a, poly_point *b) {
    return ((double) b->y - (double) a->y) / ((double) b->x - (double) a->x);
}

struct PolyPoint *getNearestEdge(struct Polygon *pPolygon, int x, int y, unsigned mode) {
    poly_point *result = NULL;
    poly_point *anchor = newPolyPoint(x, y);
    double rel_angle;
    double dist, dist_min = INFINITY;
    double dist_anchor, dist_next;
    double x1, x2, y1, y2, dot;
    int count = 0;

    poly_point *currPoint = pPolygon->head;
    poly_point *currNext;

    while (currPoint != NULL && count < pPolygon->size) {

        dist = distance(currPoint->x, currPoint->y, anchor->x, anchor->y);
        if (dist < dist_min) {
            dist_min = dist;
            result = currPoint;
        }

        currNext = currPoint->next;
        if (currNext == NULL && (mode & (DRAW_LOOP | DRAW_FILL)))
                currNext = pPolygon->head;

        if (currNext != NULL) {

            dist_anchor = distance(currPoint->x, currPoint->y, anchor->x, anchor->y);
            dist_next = distance(currPoint->x, currPoint->y, currNext->x, currNext->y);

            x1 = x - currPoint->x;
            y1 = y - currPoint->y;

            x2 = currNext->x - currPoint->x;
            y2 = currNext->y - currPoint->y;

            dot = x1 * x2 + y1 * y2;

            rel_angle = acos(dot / (dist_anchor * dist_next));

            if (cos(rel_angle) >= 0) {
                    dist = fabs(dist_anchor * sin(rel_angle));
                if (dist < dist_min) {
                    dist_min = dist;
                    result = currPoint;
                }
            }
        }
        currPoint = currPoint->next;
        count++;
    }

    free(anchor);
    return result;
}

void addMidPoint(struct Polygon *pPolygon, struct PolyPoint *point, unsigned mode) {
    poly_point *next = point->next;

    if (next == NULL && mode & (DRAW_LOOP | DRAW_FILL))
        next = pPolygon->head;

    if (next != NULL) {
        int x = (point->x + next->x) / 2;
        int y = ( point->y + next->y) / 2;

        addPoint(pPolygon, point->next, x, y);
    }
}

void addTail(struct Polygon *pPolygon, int x, int y) {
    poly_point *newPoint = newPolyPoint(x, y);
    if (pPolygon->size < 1) {
        pPolygon->head = newPoint;
        pPolygon->tail = newPoint;
    } else {
        poly_point *oldTail = pPolygon->tail;
        newPoint->prev = oldTail;
        oldTail->next = newPoint;
        pPolygon->tail = newPoint;
    }
    pPolygon->size++;

    if (y > pPolygon->ymax)
        pPolygon->ymax = y;

    if (y < pPolygon->ymin)
        pPolygon->ymin = y;
}

void addPoint(struct Polygon *pPolygon, struct PolyPoint *point, int x, int y) {
    if (point != NULL) {
        poly_point *newPoint = newPolyPoint(x, y);

        newPoint->prev = point->prev;
        newPoint->next = point;
        point->prev->next = newPoint;
        point->prev = newPoint;
        pPolygon->size++;

        if (y > pPolygon->ymax)
            pPolygon->ymax = y;

        if (y < pPolygon->ymin)
            pPolygon->ymin = y;

    } else
        addTail(pPolygon, x, y);
}

void removePolyPoint(struct Polygon *pPolygon, struct PolyPoint *point) {

    if (point == pPolygon->tail)
        pPolygon->tail = point->prev;

    if (point == pPolygon->head)
        pPolygon->head = point->next;

    if (point->next != NULL)
        point->next->prev = point->prev;

    if (point->prev != NULL)
        point->prev->next = point->next;

    pPolygon->size--;

    if (point->y == pPolygon->ymax) {
        int new_max = -1;
        poly_point *currPoint;
        for (int i = 0; i < pPolygon->size; ++i) {
            currPoint = getPoint(pPolygon, i);
            if (currPoint->y >= new_max)
                new_max = currPoint->y;
        }
        pPolygon->ymax = new_max;
    } else if (point->y == pPolygon->ymin) {
        int new_min = (int) INFINITY;
        poly_point *currPoint;
        for (int i = 0; i < pPolygon->size; ++i) {
            currPoint = getPoint(pPolygon, i);
            if (currPoint->y <= new_min)
                new_min = currPoint->y;
        }
        pPolygon->ymin = new_min;
    }

    free(point);
}

void removePointIndex(struct Polygon *pPolygon, int i) {
    removePolyPoint(pPolygon, getPoint(pPolygon, i));
}

void freePolygon(struct Polygon *pPolygon) {
    poly_point *currPoint = pPolygon->head;
    poly_point *currNext = NULL;

    while (currPoint != NULL) {
        currNext = currPoint->next;
        free(currPoint);
        currPoint = currNext;
        pPolygon->size--;
    }

    free(pPolygon);
}

void bresenhamCoord(Image *image, int xa, int ya, int xb, int yb) {
    int dx = abs(xb - xa), step_x = xa < xb ? 1 : -1;
    int dy = abs(yb - ya), step_y = ya < yb ? 1 : -1;
    int d = (dx > dy ? dx : -dy) / 2, d_tmp;

    while (xa != xb || ya != yb) {
        I_plot(image, xa, ya);
        d_tmp = d;
        if (d_tmp > -dx) {
            d -= dy;
            xa += step_x;
        }
        if (d_tmp < dy) {
            d += dx;
            ya += step_y;
        }
    }
}

void bresenham(Image *image, struct PolyPoint *a, struct PolyPoint *b) {
    bresenhamCoord(image, a->x, a->y, b->x, b->y);
}

int cmpfunc(const void *a, const void *b) {
    return (*(int *) a - *(int *) b);
}

void scan_line(Image *image, polygon *pPolygon) {
    poly_point *currPoint, *currNext;
    int intersectedX[pPolygon->size], intersectCount = 0;
    int currIntersection;
    double alpha, beta;

    for (int line = pPolygon->ymin; line <= pPolygon->ymax; line++) {

        for (int i = 0; i < pPolygon->size; ++i)
            intersectedX[i] = -1;

        intersectCount = 0;
        currIntersection = -1;
        currPoint = pPolygon->head;

        for (int i = 0; i < pPolygon->size; ++i) {

            currNext = currPoint->next;

            if (currNext == NULL)
                currNext = pPolygon->head;

            alpha = compute_alpha(currPoint, currNext);
            beta = (double) currPoint->y - (double) currPoint->x * alpha;

            if (alpha != 0) // lines are not parallel
                currIntersection = (int) round((0.5 + (double) line - beta) / alpha);

            if (currIntersection <= fmax(currPoint->x, currNext->x) &&
                currIntersection >= fmin(currPoint->x, currNext->x) &&
                line < fmax(currPoint->y, currNext->y) &&
                line >= fmin(currPoint->y, currNext->y)) {

                intersectedX[intersectCount] = currIntersection;
                intersectCount++;
            }
            currPoint = currPoint->next;
        }

        qsort(intersectedX, intersectCount, sizeof(int), cmpfunc);
        for (int j = 0; j < intersectCount / 2; ++j)
            for (int i = intersectedX[2 * j]; i <= intersectedX[2 * j + 1]; ++i)
                I_plot(image, i, line);
    }
}

void I_polygon(Image *image, polygon *pPolygon, unsigned mode) {
    poly_point *currPoint = pPolygon->head;
    if (pPolygon->size > 1) {
        while (currPoint != NULL) {
            if (currPoint->next != NULL)
                bresenham(image, currPoint, currPoint->next);
            else if (mode & DRAW_LOOP)
                bresenham(image, currPoint, pPolygon->head);
            currPoint = currPoint->next;
        }

        if (pPolygon->size > 2 && (mode & DRAW_FILL))
            scan_line(image, pPolygon);
    }

    currPoint = pPolygon->head;
    if (mode & DRAW_POINT)
        while (currPoint != NULL) {
            I_point(image, currPoint, 3);
            currPoint = currPoint->next;
        }
}

void I_point(Image *img, struct PolyPoint *pPoint, int size) {
    for (int i = pPoint->y - size; i < pPoint->y + size; ++i)
        for (int j = pPoint->x - size; j < pPoint->x + size; ++j)
            I_plotColor(img, j, i, C_new(1, 0, 0));
}

void I_edge(Image *img, struct Polygon *pPolygon, struct PolyPoint *pPoint, unsigned mode) {
    poly_point *next = pPoint->next;

    if (next == NULL && mode & (DRAW_LOOP | DRAW_FILL))
        next = pPolygon->head;

    if (next != NULL) {
        Color old = img->_current_color;
        img->_current_color = C_new(1, 0, 0);
        bresenham(img, pPoint, next);
        img->_current_color = old;
    }

}
